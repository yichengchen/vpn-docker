FROM alpine:3.11 as builder
ARG OPENFORTIVPN_VERSION=v1.16.0

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories &&\
  apk add --no-cache \
    autoconf automake build-base ca-certificates curl git openssl-dev ppp && \
  update-ca-certificates 2>/dev/null || true && \
  # build openfortivpn
  mkdir -p /usr/src/openfortivpn && \
  curl -sL https://github.com/adrienverge/openfortivpn/archive/${OPENFORTIVPN_VERSION}.tar.gz \
    | tar xz -C /usr/src/openfortivpn --strip-components=1 && \
  cd /usr/src/openfortivpn && \
  ./autogen.sh && \
  ./configure --prefix=/usr --sysconfdir=/etc && \
  make -j$(nproc) && \
  make install

FROM dreamacro/clash:latest as clash
COPY entrypoint.sh /root/
RUN chmod +x /root/entrypoint.sh


FROM alpine
ENV USERNAME ""
ENV PASSWORD ""
ENV SERVER_ADDR ""
ENV TOTP_KEY ""
ENV CERT ""

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories &&\
apk add --no-cache ca-certificates openssl ppp oath-toolkit-oathtool &&\
echo lcp-echo-interval 60 >> /etc/ppp/options
COPY --from=clash /root/.config/clash/Country.mmdb /root/.config/clash/
COPY --from=clash /clash /root/entrypoint.sh /usr/bin/

COPY --from=builder /usr/bin/openfortivpn /usr/bin/
COPY config.yaml /root/.config/clash/
ENTRYPOINT ["/usr/bin/entrypoint.sh"]
