#!/bin/sh
/usr/bin/clash &
echo "clash start at :7891"
/usr/bin/openfortivpn $SERVER_ADDR -u $USERNAME -p $PASSWORD --trusted-cert $CERT --half-internet-routes=1 --otp=`oathtool --totp -b $TOTP_KEY`
